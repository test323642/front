/** @type {import('tailwindcss').Config} */
export default {
  //purge: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  //content: ["src/"],
  theme: {
    extend: {},
  },
  plugins: [require("@tailwindcss/typography")],
};
