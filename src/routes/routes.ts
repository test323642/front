import {RouteRecordRaw} from 'vue-router';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'QUESTION 1',
    component: () => import('../components/Pages/Q1.vue'),
  },
  {
    path: '/q2',
    name: 'QUESTION 2',
    component: () => import('../components/Pages/Q2.vue'),
  },
  {
    path: '/q3',
    name: 'QUESTION 3',
    component: () => import('../components/Pages/Q3.vue'),
  },
  {
    path: '/q4',
    name: 'QUESTION 4',
    component: () => import('../components/Pages/Q4.vue'),
  },
  {
    path: '/q5',
    name: 'QUESTION 5',
    component: () => import('../components/Pages/Q5.vue'),
  },
  {
    path: '/q6',
    name: 'QUESTION 6',
    component: () => import('../components/Pages/Q6.vue'),
  },

  {
    path: '/users',
    name: 'Users',
    component: () => import('../components/Pages/Users.vue'),
  },

]
export default routes;