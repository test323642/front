import { ref } from "vue";

export function useUser() {
  const API_URL = "https://back-c2bfc2xnza-as.a.run.app/api";

  const curuser = ref({});
  const users = ref([]);
  const page = ref(1);
  const errormessage = ref("");
  const nextpage = () => {
    page.value++;
    getUsers();
  };
  const prevpage = () => {
    if (page.value > 1) {
      page.value--;
      getUsers();
    }
  };

  const getUsers = async () => {
    // this effect will run immediately and then
    // re-run whenever currentBranch.value changes
    const url = `${API_URL}/users?page=${page.value}`;
    users.value = await (await fetch(url)).json();
    return users.value;
  };
  const deleteUser = async () => {
    const url = `${API_URL}/users/` + curuser.value.id + "";
    const api_res = await fetch(url, { method: "DELETE" });
    let data = await api_res.body;
    getUsers();
    curuser.value = {};
  };
  const saveUser = async () => {
    let apires = {};
    let url = `${API_URL}/users`;
    let method = "POST";

    if (curuser.value.id) {
      url = `${API_URL}/users/` + curuser.value.id + "";
      method = "PUT";
    }
    errormessage.value = "";

    const api_res = await fetch(url, {
      method: method,
      body: JSON.stringify(curuser.value),
      headers: {
        "Content-Type": "application/json",
      },
    });
    if (api_res.ok) {
      let data = await api_res.json();
      getUsers();
      curuser.value = {};
      //editOpen.value = false;
    } else {
      let data = await api_res.json();
      Object.entries(data).forEach((entry) => {
        const [key, value] = entry;
        errormessage.value += key + ": " + value + "<br>";
      });
    }

    /*
	  then((response) => {
      if (response.ok) {
        getUsers();
        curuser.value = {};
        //editOpen.value = false;
      } else {
        response.json().then((data) => {
          Object.entries(data).forEach((entry) => {
            const [key, value] = entry;
            errormessage.value += key + ": " + value + "<br>";
          });
        });
      }
    });
	*/
  };
  return {
    errormessage,
    curuser,
    getUsers,
    users,
    nextpage,
    deleteUser,
    prevpage,
    saveUser,
    page,
  };
}
